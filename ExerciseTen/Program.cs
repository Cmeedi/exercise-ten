﻿using System;
using System.IO;

namespace ExerciseTen
{
    class Program
    {
        static void Main(string[] args)
        {
            //This try statement tests our program for errors
            try
            {
                //We create a StreamReader object called "inputFile"
                StreamReader inputFile;
                string line;
                int count = 0;
                int total;
                double average;

                //This array holds the ',' delimeter
                char[] delim = { ',' };
                string[] words = new string [10];
                //InputFile now opens our "exTenText.txt" file
                inputFile = File.OpenText("exTenText.txt");

                while (!inputFile.EndOfStream)
                {
                    count++;

                    line = inputFile.ReadLine();
                    

                    string[] tokens = line.Split(null);

                    

                    total = 0;
                    foreach(string str in tokens)
                    {
                        if (str.Contains('e'))
                        {
                            total++;
                           Console.WriteLine(str + " incrememts the count to " + total);
                        }
                        else if (str.Contains('t'))
                        {
                            total++;
                            Console.WriteLine(str + " increments the count to " + total);
                        }
                    }


                }


            }
            catch(Exception ex)
            {
                Console.WriteLine("Error");
            }
        }
    }
}
